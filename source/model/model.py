# Adapted from https://github.com/ryansmcgee/seirsplus

import random
from seirsplus.models import *
import networkx
from parameters import *
from degreeDistribution import *

# Generate graph with subset of better behaving pop
def generateGraphWithXPercentOfPopImitatingUser(G, percentage, numNodes, userDegree):
    G_imitate = G.copy()
    nodeList = networkx.nodes(G_imitate)
    nodeSubset = random.sample(nodeList, percentage * numNodes // 100)
    for n in nodeSubset:
        edgesOfNode = list(networkx.dfs_edges(G_imitate, source=n, depth_limit=1))
        random.shuffle(edgesOfNode)
        while len(edgesOfNode) > userDegree:
            edge = edgesOfNode.pop()
            G_imitate.remove_edge(edge[0], edge[1])
    return G_imitate


# Launch simulation with user defined data
def simulationUser(userNumberOfContacts, country, duration):
    # userNumberOfContacts: int Number of close contacts (that might spread the virus) during the day
    # country: string Name of the country
    # duration: int Number of days for the simulation
    # return int, int Number of infections avoided if 10% of pop has only userNumberOfContacts per day, Number of deaths avoided if 10% of pop has only userNumberOfContacts per day
    totalPop, numNodes, degree, userDegree, sigma, gamma, mu_I, beta, p, initI, initE = getParam(userNumberOfContacts, country)

    baseGraph    = networkx.barabasi_albert_graph(n=numNodes, m=degree)
    G_normal     = custom_exponential_graph(baseGraph, scale=100)

    model = SEIRSNetworkModel(G=G_normal, beta=beta, sigma=sigma, gamma=gamma, mu_I=mu_I, p=p, initI=initI)

    model.run(T=duration)

    I = model.numI
    F = model.numF

    G_imitate = generateGraphWithXPercentOfPopImitatingUser(G_normal, 10, numNodes, userDegree)

    model = SEIRSNetworkModel(G=G_imitate, beta=beta, sigma=sigma, gamma=gamma, mu_I=mu_I, p=p, initI=initI)

    model.run(T=duration)

    I_user = model.numI
    F_user = model.numF

    I_finalDiff = (I[len(I) - 1] - I_user[len(I_user) - 1]) * totalPop // numNodes
    F_finalDiff = (F[len(I) - 1] - F_user[len(F_user) - 1]) * totalPop // numNodes

    return I_finalDiff, F_finalDiff

    # model.figure_infections(plot_D_I = False, plot_F = "line")


#print(simulationUser(3, "France", 60))
