
# Set parameters for the SEIRS model

import datetime
import urllib.request
import pandas as pd
import numpy as np
import json

def response(url):
    with urllib.request.urlopen(url) as response:
        return response.read()


def getParam(userNumberOfContacts, country):

    # Get country name
    countryName = country

    # Up-to-date national Covid19 numbers
    # Extracted from Worldwide Data repository operated by the Johns Hopkins University Center for Systems Science and Engineering (JHU CSSE)
    # Using https://covid19api.com
    urlCountryConfirmed = "https://api.covid19api.com/total/country/" + str.lower(countryName).replace(" ", "-") + "/status/confirmed"
    countryConfirmed = json.loads(response(urlCountryConfirmed))
    urlCountryDead = "https://api.covid19api.com/total/country/" + str.lower(countryName).replace(" ", "-") + "/status/deaths"
    countryDead = json.loads(response(urlCountryDead))

    # Country demographic info extracted from https://github.com/samayo/country-json
    urlCountryPop = "https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-population.json"
    countryPop = response(urlCountryPop)
    countryPopFrame = pd.DataFrame(json.loads(countryPop))
    countryPopDict = pd.Series(countryPopFrame.population.values,index=countryPopFrame.country).to_dict()

    # Country mortality
    # Extracted from https://worldpopulationreview.com/countries/death-rate-by-country/ on 10/04/20
    countryMortality = dict()
    with open('../data/mortality.json') as json_file:
        countryMortality = pd.DataFrame(json.load(json_file)["data"])
    countryMortalityDict = pd.Series(countryMortality.Rate.values,index=countryMortality.name).to_dict()

    # Date of first confirmed case for country
    urlCountryFeed = "https://api.covid19api.com/dayone/country/" + str.lower(countryName).replace(" ", "-") + "/status/confirmed"
    countryFeed = json.loads(response(urlCountryFeed))
    countryFeedFrame = pd.DataFrame(countryFeed)
    countryFeedFrame["Date"] = pd.to_datetime(countryFeedFrame["Date"])
    countryFeedFrame = countryFeedFrame.sort_values("Date")
    countryFirstConfirmedDate = countryFeedFrame.iloc[0]["Date"]

    # Number of nodes and degrees in the network
    numNodes = 5000 # Recommended 10000
    degree = 20
    userDegree = userNumberOfContacts # Computed from the user data

    # Progression rate
    timeFromInfectionToInfectiousness = np.mean([3.69, 3.6, 3.44]) # Extracted from https://midasnetwork.us/covid-19/#resources on 10/04/20
    sigma = 1 / timeFromInfectionToInfectiousness
    print("sigma %f", sigma)

    # Recovery rate
    timeFromSymptomsToRecovery = np.mean([20.3, 21.2, 17.5, 19.1, 19.2, 19.2, 21.6, 22.4, 22.9]) # Extracted from https://midasnetwork.us/covid-19/#resources on 10/04/20
    incubationPeriod = 5.2
    gamma = 1 / (incubationPeriod + timeFromSymptomsToRecovery)
    print("gamma %f", gamma)

    # Mortality rate of infected
    officialInfected = countryConfirmed[-1]["Cases"]
    officialDead = countryDead[-1]["Cases"]
    timeSinceFirstConfirmed = datetime.datetime.utcnow() - countryFirstConfirmedDate.replace(tzinfo=None)
    daysSinceFirstConfirmed = timeSinceFirstConfirmed.days
    mu_I = officialDead / officialInfected / daysSinceFirstConfirmed
    print("mu_I %f", mu_I)

    # Transmission rate
    beta = np.mean([1.12, 0.52, 0.35]) # Extracted from https://midasnetwork.us/covid-19/#resources on 10/04/20
    lockdownEffect = 0.5 # Estimated effect of lockdown measures on the transmission rate, extracted from https://www.imperial.ac.uk/media/imperial-college/medicine/mrc-gida/2020-03-30-COVID19-Report-13.pdf on 10/04/20
    # beta = beta * lockdownEffect
    print("beta %f", beta)

    # Probability of global interactions
    p = 0
    print("p %f", p)

    # Number of infected and exposed people at day D
    totalPop = int(countryPopDict[countryName])
    initI = officialInfected * numNodes // totalPop
    print("initI %f", initI)
    initE = initI * beta * 3 # Rough estimate
    print("initE %f", initE)

    return totalPop, numNodes, degree, userDegree, sigma, gamma, mu_I, beta, p, initI, initE
