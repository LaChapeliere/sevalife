
from model import *
import pandas as pd


computedValues = pd.DataFrame(columns=["country", "userNumberOfContacts", "infectedDiff", "deathDiff"])

for country in ["France", "United Kingdom", "United States", "Spain"]:
    for contactNb in range(0, 20, 4):
        print("Computing for %d in %s" % (contactNb, country))
        I_finalDiff, F_finalDiff = simulationUser(contactNb, country, 60)
        newRow = {"country": country, "userNumberOfContacts": contactNb, "infectedDiff": I_finalDiff, "deathDiff": F_finalDiff}
        computedValues = computedValues.append(newRow, ignore_index=True)

computedValues.to_csv("../data/hardCoded.csv", index=False)
